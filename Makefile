# Makefile

# compiler
FC=gfortran
FFLAGS= -fopenmp

# folders names
SRC=src
OBJ=obj
BIN=bin

# source and object files
SRCS=$(notdir $(wildcard $(SRC)/*.f90))
OBJS=$(notdir $(patsubst $(SRC)/%.f90, $(OBJ)/%.o, $(wildcard $(SRC)/*.f90)))

# executable name
NEXE=exeprog

# target to all targets
all: build $(NEXE) 

# create executable
$(NEXE): $(OBJS)
	$(FC) $(FFLAGS) -o $(BIN)/$@ $(addprefix $(OBJ)/, $^)

# create objects
%.o: $(SRC)/%.f90
	$(FC) $(FFLAGS) -I$(OBJ) -J$(OBJ) -c $< -o $(OBJ)/$@

# built needed directory tree
build:
ifneq ($(wildcard $(OBJ)),)
	@echo 'Directory' $(OBJ) 'found'
else
	@echo 'Directory' $(OBJ) 'not found, making it ...'
	@mkdir $(OBJ)
endif

ifneq ($(wildcard $(BIN)),)
	@echo 'Directory' $(BIN) 'found'
else
	@echo 'Directory' $(BIN) 'not found, making it ...'
	@mkdir $(BIN)
endif

# dump debug info
debug:
	@echo $(SRCS)
	@echo $(OBJS)

# remove executable and objects
clean:
	rm $(OBJ)/*.o $(OBJ)/*.mod $(BIN)/$(NEXE)

# remove build directories
cleanall:
	rm -rf $(OBJ) $(BIN)
